# soundcloudmpd

I love SoundCloud - I love mpd. Let's try and bridge the two!

## Features

- [x] Like/Unlike
- [ ] Follow/Unfollow
- [ ] Search
- [x] Add stream to mpd
- [x] Add related tracks to mpd

## Dependencies

* A POSIX shell
* [curl](https://curl.haxx.se)
* [jq](https://stedolan.github.io/jq/)
* [youtube-dl](https://github.com/rg3/youtube-dl/)
* [pycookiecheat](https://github.com/n8henrie/pycookiecheat) (install it via `python3 -m pip install --user pycookiecheat`)
* [mpd](https://www.musicpd.org) (with the soundcloud plugin built and enabled)
* [mpc](https://www.musicpd.org/clients/mpc/)

## Usage

In order to use soundcloudmpd.sh you will need to login to SoundCloud in Google Chrome or Chromium (this is because we retrieve your cookies in order to be able to communicate with SoundCloud). We default to Chromium, if you use Google Chrome you'll need to set the `CHROME_BROWSER` variable to "chrome".

`soundcloudmpd.sh like`   - Like the currently playing song

`soundcloudmpd.sh unlike` - Unlike the currently playing song

`soundcloudmpd.sh stream` - Add the 1st 100 items of your stream to `mpd`

`soundcloudmpd.sh stream --continue` - Add the next 100 items of your stream to `mpd`

`soundcloudmpd.sh dlsong` - Download the currently playing song using `youtube-dl`
