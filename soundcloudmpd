#!/bin/sh

usage() {
  prog="$(basename "$0")"
  cat <<EOF
  $prog - A bridge between soundcloud and mpd

  $prog like - Like the currently playing track
  $prog unlike - Unlike the currently playing track
  $prog stream - Add the 1st 100 items of your stream to mpd
  $prog stream --continue - Add the next 100 items of your stream to mpd
  $prog dlsong - Download the currently playing song using youtube-dl
EOF
}

[ -z "$1" ] && {
  usage
  exit 1
}

[ "$1" = "--help" ] || [ "$1" = "-help" ] || [ "$1" = "-h" ] && {
  usage
  exit 0
}

command -v parallel > /dev/null && HAS_PARALLEL=1 || HAS_PARALLEL=0

CHROME_BROWSER="${CHROME_BROWSER:-chromium}"

APP_VERSION="${APP_VERSION-1525356390}"

[ -z "$API_KEYS" ] && API_KEYS="LvWovRaJZlWCHql0bISuum8Bd2KX79mb
WQjdiVSzJg68EdarsMiaCqxNuaEXcd1K
"

XDG_CACHE_HOME="${XDG_CACHE_HOME:-~/.cache}"

mkdir -p "$XDG_CACHE_HOME" || exit 1

#Grabs the CLIENT_ID from youtube-dl
#find /usr/lib/python* -name "*youtube*dl*" -type d | grep -v -E '.*.egg-info$' | tail -1 | xargs -r -I'##' find "##" -name "soundcloud.py" -exec grep -E "_CLIENT_ID = '.*'" {} \; | head -1 | grep -ohE "'.*'" | cut -c2- | rev | cut -c 2- | rev | xargs -r -I'{}' printf "_CLIENT_ID='%s'" "{}"

get_random_api_key() {
  f="$(seq 1 2 | shuf | head -1)"
  [ "$f" -eq 1 ] && func="head" || func="tail"
  printf "%s" "$API_KEYS" | shuf | $func -1
}

API_KEY="${API_KEY:-$(get_random_api_key)}"

get_chrome_version_number() {
  V="$($CHROME_BROWSER --version | rev | awk '{print $1}' | rev)"
  [ -z "$V" ] && V="67.0.3396.30"

  printf "%s" "$V"
}

USER_AGENT="${USER_AGENT:-Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/$(get_chrome_version_number) Safari/537.36}"
 
get_oauth_key() {
  python3 -c "from pycookiecheat import chrome_cookies;cookies=chrome_cookies('https://soundcloud.com',None,'$CHROME_BROWSER');print(cookies['oauth_token'])"
}

# This is what I'm calling the "short user id" (I don't know if soundcloud has a proper name for it)
# This is needed when liking/unliking a track and possibly in other places
get_short_user_id() {
  python3 -c "from pycookiecheat import chrome_cookies;cookies=chrome_cookies('https://soundcloud.com',None,'$CHROME_BROWSER');print(cookies['i'])"
}

get_user_id() {
  JSON="$(curl -s "https://api-v2.soundcloud.com/dashbox/stream?device_locale=en&locale=en&client_id=$API_KEY&app_version=$APP_VERSION&app_locale=en" \
    -H "Authorization: OAuth $(get_oauth_key)" \
    -H 'Origin: https://soundcloud.com' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
    -H "User-Agent: $USER_AGENT" \
    -H 'Accept: application/json, text/javascript, */*; q=0.01' \
    -H 'Referer: https://soundcloud.com/' \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' \
    --compressed)"

  printf "USER_ID=%s;" "$(printf "%s" "$JSON" | jq -r '.user_id | @sh')"
  printf "SC_A_ID=%s;" "$(printf "%s" "$JSON" | jq -r '.sc_a_id | @sh')"
}

resolve() {
  JSON="$(curl -s "https://api.soundcloud.com/resolve.json?url=$1&device_locale=en&locale=en&client_id=$API_KEY&app_version=$APP_VERSION&app_locale=en" \
    -H "Authorization: OAuth $(get_oauth_key)" \
    -H 'Origin: https://soundcloud.com' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
    -H "User-Agent: $USER_AGENT" \
    -H 'Accept: application/json, text/javascript, */*; q=0.01' \
    -H 'Referer: https://soundcloud.com/' \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' \
    --compressed)"
}

get_song() {
  SONG="$(mpc -f '%file%')"
  # To Do:
  #     * Make sure this regex is correct. Are there other URL's that are valid?
  if ! printf "%s" "$SONG" | grep -q -E 'http(s)?://(api.)?soundcloud.com'
  then
    return 1
  fi

  # To Do:
  #     * I'm lazy, this should be replaced with the resolve function
  youtube-dl --dump-json "$SONG"
}

open() {
  URL="$(get_song | jq -r '.webpage_url')"
  [ -z "$URL" ] && return 1

  xdg-open "$URL"
}

[ "$1" = "open" ] && {
  "$1"
  exit $?
}

_like() {
  SONG_ID="$(get_song | jq -r '.id')"
  [ -z "$SONG_ID" ] && return 1
  curl -s "https://api-v2.soundcloud.com/users/$(get_short_user_id)/track_likes/$SONG_ID?client_id=$API_KEY&app_version=$APP_VERSION&app_locale=en" \
    -X "$1" \
    -H 'Origin: https://soundcloud.com' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
    -H "Authorization: OAuth $(get_oauth_key)" \
    -H 'Content-Type: application/json' \
    -H 'Accept: application/json, text/javascript, */*; q=0.1' \
    -H 'Referer: https://soundcloud.com/' \
    -H "User-Agent: $USER_AGENT" \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' \
    --data-binary 'null' --compressed
}

like() {
  _like PUT
}

unlike() {
  _like DELETE
}

[ "$1" = "like" ] || [ "$1" = "unlike" ] && {
  "$1"
  exit $?
}

stream() {
  eval "$(get_user_id)"
  LIMIT="${LIMIT:-100}"

  [ "$1" = "--continue" ] && {
    [ -f "$XDG_CACHE_HOME/soundcloudmpd" ] && OFFSET="$(cat "$XDG_CACHE_HOME/soundcloudmpd")"
  }

  OFFSET="${OFFSET:-0}"

  JSON="$(curl -s "https://api-v2.soundcloud.com/stream?sc_a_id=$SC_A_ID&device_locale=en&user_urn=soundcloud%3Ausers%3A$(get_short_user_id)&promoted_playlist=true&client_id=$API_KEY&limit=$LIMIT&offset=$OFFSET&linked_partitioning=1&app_version=$APP_VERSION&app_locale=en" \
    -H "Authorization: OAuth $(get_oauth_key)" \
    -H 'Origin: https://soundcloud.com' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
    -H "User-Agent: $USER_AGENT" \
    -H 'Accept: application/json, text/javascript, */*; q=0.01' \
    -H 'Referer: https://soundcloud.com/' \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' \
    --compressed)"

  eval "$(printf "NEXT_HREF=%s" "$(printf "%s" "$JSON" | jq -r '.next_href | @sh')")"

  if [ "$HAS_PARALLEL" -eq 0 ]
  then
    printf "%s" "$JSON" | jq -r '.[][]? | .track?,.playlist? | if .permalink_url then (.permalink_url) else empty end' | while read -r line
    do
      mpc load "soundcloud://url/$line"
    done
  else
    printf "%s" "$JSON" | jq -r '.[][]? | .track?,.playlist? | if .permalink_url then (.permalink_url) else empty end' | parallel -r -k "mpc load 'soundcloud://url/{}'"
  fi

  printf "%s" "$NEXT_HREF" | grep -oE '&offset=.*' | sed -e 's/offset=//g' -e 's/&/ /g' | awk '{print $1}' > "$XDG_CACHE_HOME/soundcloudmpd"
}

[ "$1" = "stream" ] && {
  "$@"
  exit "$?"
}

dlsong() {
  [ -z "$HOME" ] && exit 1
  XDG_MUSIC_DIR="${XDG_MUSIC_DIR:-$HOME/Music}"
  mkdir -p "$XDG_MUSIC_DIR" || exit 1
  cd "$XDG_MUSIC_DIR" || exit 1
  youtube-dl -x --add-metadata "$(mpc -f '%file%')"
}

[ "$1" = "dlsong" ] && {
  "$@"
  exit "$?"
}

related() {
  SONG_ID="$(get_song | jq -r '.id')"
  [ -z "$SONG_ID" ] && return 1
  LIMIT="${LIMIT:-100}"

  [ "$1" = "--continue" ] && {
    [ -f "$XDG_CACHE_HOME/soundcloudmpd_related" ] && OFFSET="$(cat "$XDG_CACHE_HOME/soundcloudmpd_related")"
  }

  OFFSET="${OFFSET:-0}"

  JSON="$(curl -s "https://api-v2.soundcloud.com/tracks/$SONG_ID/related?user_id=$(get_short_user_id)&client_id=$API_KEY&limit=$LIMIT&offset=$OFFSET&linked_partitioning=1&app_version=$APP_VERSION&app_locale=en" \
    -H "Authorization: OAuth $(get_oauth_key)" \
    -H 'Origin: https://soundcloud.com' \
    -H 'Accept-Encoding: gzip, deflate, br' \
    -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8' \
    -H "User-Agent: $USER_AGENT" \
    -H 'Accept: application/json, text/javascript, */*; q=0.01' \
    -H 'Referer: https://soundcloud.com/' \
    -H 'Connection: keep-alive' \
    -H 'DNT: 1' --compressed)"

  eval "$(printf "NEXT_HREF=%s" "$(printf "%s" "$JSON" | jq -r '.next_href | @sh')")"

  if [ "$HAS_PARALLEL" -eq 0 ]
  then
    printf "%s" "$JSON" | jq -r '.collection[].permalink_url' | while read -r line
    do
      mpc load "soundcloud://url/$line"
    done
  else
    printf "%s" "$JSON" | jq -r '.collection[].permalink_url' | parallel -r -k "mpc load 'soundcloud://url/{}'"
  fi

  printf "%s" "$NEXT_HREF" | grep -oE '&offset=.*' | sed -e 's/offset=//g' -e 's/&/ /g' | awk '{print $1}' > "$XDG_CACHE_HOME/soundcloudmpd_related"
}

[ "$1" = "related" ] && {
  "$@"
  exit "$?"
}
